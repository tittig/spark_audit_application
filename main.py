# import 
"""
@author: Gimenez Thibault
@description: analysis of spark2-history
"""
import sys
import os
import json
import time
import argparse
import importlib
import requests
from src.read_config import read_conf
from src.EventLog import EventLog
from src.Singleton_logger import *
from requests_kerberos import HTTPKerberosAuth
from multiprocessing.dummy import Pool as ThreadPool 
from datetime import datetime

#fonction pour recuperer les configuration
def get_conf(args) :
    app_configs = read_conf().load_json_config(args)
    spark_conf_path = app_configs['SPARK_CONF_DIR'] + "/spark-defaults.conf"
    spark_config = read_conf().read_spark_conf(spark_conf_path)
    return app_configs, spark_config

#fonction pour recuperer une liste d'application 
def list_application_to_analysis(spark_config, minEndDate, maxEndDate):
    if "spark.history.kerberos.enabled" in spark_config['spark_section'] and spark_config.get('spark_section','spark.history.kerberos.enabled') == "true" :
        session = requests.Session()
        session.trust_env = False
        r = session.get("https://"+spark_config.get('spark_section','spark.yarn.historyServer.address')+"/api/v1/applications?status=completed&minEndDate="+minEndDate+"&maxEndDate="+maxEndDate, auth=HTTPKerberosAuth(mutual_authentication="OPTIONAL"), verify=False )
        if r.status_code == requests.codes.ok :
            myjson = json.loads(r.text)
            r.close
            return myjson
        else :
            r.raise_for_status()
            r.close()
    else : 
        #TO DO
        print("No implemented")
        
#fonction d'audit de la liste des applications
def audit_application(mylist) :
    ev = EventLog(spark_config, mylist["id"])
    ev.basic_SparkEvent_audit()
    if "allocated_memory" in ev.basic_SparkEvent_audit() : #check if audit json isn't empty
        #elactic_l.append(mydict)
        elactic_l.append(ev.basic_SparkEvent_audit())
        
if __name__ == '__main__':
    pool = ThreadPool(1) 
    elactic_l = [] #list of json to index in elastic
    mydict = { "index": {}} #dict to add for elastic bulk
    
    # initiate the parser
    parser = argparse.ArgumentParser()
    # add long and short argument
    parser.add_argument("--minEndDate", "-min", help="set output minEndDate")
    parser.add_argument("--maxEndDate", "-max", help="set output maxEndDate")
    parser.add_argument("--config", "-conf", help="set output configfile")   
    # read arguments from the command line
    args = parser.parse_args()
    minEndDate = args.minEndDate
    maxEndDate = args.maxEndDate
    log_conf = args.config
       
    #get configuration and create logger
    app_configs, spark_config = get_conf(log_conf)
    logger = MyLogger.__call__().get_logger()
    logger.handlers[1].doRollover() # force rotate at start
    logger.info("START WITH PARAMETERS : minEndDate:%s | maxEndDate:%s | log_conf:%s", minEndDate, maxEndDate, log_conf)
    
    try:        
        #get applications list
        logger.info('Start to get list of application to analyze')
        mylist = list_application_to_analysis(spark_config, minEndDate, maxEndDate)
        logger.info('List size : ' + str(len(mylist)))
        
        #analyse de la liste d'application
        logger.info('Start analyze of the list')
        start_time = time.time()
        pool.map(audit_application, mylist)
        pool.close() #close the pool and wait for the work to finish 
        pool.join() 
        logger.info('End analyze of the list ' + "--- %s seconds ---" % (time.time() - start_time))
        final_l = []
        for el in elactic_l :
            final_l.append(mydict)
            final_l.append(el)
        bulk = '\n'.join(json.dumps(d) for d in final_l )+'\n'

        #index to elasticsearch
        date_time_obj = datetime.strptime(minEndDate, '%Y-%m-%dT%H:%M:%S.%fGMT')
        index=str(date_time_obj.strftime('%Y'))+"_"+str(date_time_obj.strftime('%m'))
        url = "https://"+app_configs['elk_host']+":"+app_configs['elk_port']+"/monitoring-spark-application-audit_"+index+"/applications/_bulk?pretty"
        credential = (app_configs['elk_user'],app_configs['elk_password'])
        logger.info('Index to elasticsearch : ' + url)
        logger.info('Bulk to elasticsearch : ' + bulk)
        r = EventLog.post_elk(url, credential, bulk)
        logger.info("Status of index to elasticsearch : "+url+" "+str(r.status_code) )
        
    except requests.exceptions.RequestException :
        e = sys.exc_info()[1]
        logger.info(str(e))
    finally :
        EventLog._close_session()