# Dr. Spark

**Dr. Spark** is a performance monitoring and tuning tool for Spark. It enables to get spark metrics, runs analysis on them, and presents them in grafana from spark event log. Its goal is to improve developer productivity and increase cluster efficiency by making it easier to tune the jobs.

## 0. Prerequisite
* Spark2.3
* Elasticsearch7
* Grafana6.2
* Python virtual env
``` yml
name: monitoring
channels:
  - anaconda
  - defaults
dependencies:
  - _libgcc_mutex=0.1=main
  - asn1crypto=1.2.0=py37_0
  - attrs=19.3.0=py_0
  - ca-certificates=2019.11.27=0
  - certifi=2019.11.28=py37_0
  - cffi=1.13.2=py37h2e261b9_0
  - chardet=3.0.4=py37_1003
  - cryptography=2.8=py37h1ba5d50_0
  - idna=2.8=py37_0
  - importlib_metadata=1.3.0=py37_0
  - krb5=1.16.1=h173b8e3_7
  - libedit=3.1.20181209=hc058e9b_0
  - libffi=3.2.1=hd88cf55_4
  - libgcc-ng=9.1.0=hdf63c60_0
  - libstdcxx-ng=9.1.0=hdf63c60_0
  - more-itertools=8.0.2=py_0
  - ncurses=6.1=he6710b0_1
  - openssl=1.1.1=h7b6447c_0
  - packaging=20.0=py_0
  - pip=19.3.1=py37_0
  - pluggy=0.13.1=py37_0
  - py=1.8.1=py_0
  - pycparser=2.19=py37_0
  - pykerberos=1.2.1=py37h14c3975_0
  - pyopenssl=19.1.0=py37_0
  - pyparsing=2.4.6=py_0
  - pysocks=1.7.1=py37_0
  - pytest=5.3.2=py37_0
  - python=3.7.5=h0371630_0
  - readline=7.0=h7b6447c_5
  - requests=2.22.0=py37_1
  - requests-kerberos=0.12.0=py37_0
  - setuptools=42.0.2=py37_0
  - six=1.13.0=py37_0
  - sqlite=3.30.1=h7b6447c_0
  - tk=8.6.8=hbc83047_0
  - urllib3=1.25.7=py37_0
  - wcwidth=0.1.7=py37_0
  - wheel=0.33.6=py37_0
  - xz=5.2.4=h14c3975_4
  - zipp=0.6.0=py_0
  - zlib=1.2.11=h7b6447c_3
prefix: /var/dlk/lib/miniconda3/envs/monitoring
```

## 1. Activate eventLog
The spark jobs themselves must be configured to log events, and to log them to the same shared, writable directory. Configure the Spark Event Log Service for the application in $SPARK_HOME/conf/spark_default.conf. For example, if the server was configured with a log directory of hdfs://namenode/shared/spark-logs, then the client-side options would be:
*  spark.eventLog.enabled true
*  spark.eventLog.dir hdfs://namenode/shared/spark-logs
*  park.eventLog.compress  true

spark.eventLog.enabled: Must be enabled; this property is used to reconstruct the web UI after the application has completed.
spark.eventLog.dir: This is the directory where event log information of an application is saved. It can be an HDFS path schema beginning with hdfs://, or a path schema for IBM Spectrum Scale beginning with gpfs://. The value can also be a local path beginning with file://; the default value is file:///tmp/spark-events. The directory should be created in advance.
(Optional) spark.eventLog.compress: Specifies whether to compress Spark logged events. Snappy is used as the default compression algorithm.

## 2. EventLog content
```json
{"EventType","EventContent""}
```
For example : 
```json
{"Event":"SparkListenerBlockManagerAdded","Block Manager ID":{"Executor ID":"driver","Host":"myhost.example.fr","Port":16689},"Maximum Memory":5538054144,"Timestamp":1578971791340,"Maximum Onheap Memory":5538054144,"Maximum Offheap Memory":0}
```
EventType list :
*  Stage : SparkListenerStageSubmitted, SparkListenerStageCompleted, SparkListenerStageExecutorMetrics
*  Task : SparkListenerTaskStart, SparkListenerTaskGettingResult, SparkListenerSpeculativeTaskSubmitted, SparkListenerTaskEnd
*  Job : SparkListenerJobStart, SparkListenerJobEnd
*  Environment : SparkListenerEnvironmentUpdate
*  BlockManager : SparkListenerBlockManagerAdded, SparkListenerBlockManagerRemoved
*  RDD : SparkListenerUnpersistRDD
*  Executor : SparkListenerExecutorAdded, SparkListenerExecutorRemoved, SparkListenerExecutorBlacklisted, SparkListenerExecutorUnblacklisted, SparkListenerExecutorBlacklistedForStage, SparkListenerExecutorMetricsUpdate
*  Node : SparkListenerNodeBlacklisted, SparkListenerNodeUnblacklisted, SparkListenerNodeBlacklistedForStage
*  Application : SparkListenerApplicationStart, SparkListenerApplicationEnd
*  ...

Two means to get eventLog :
*  In  hdfs : hdfs://{spark.eventLog.dir}
*  In sparkHistory : http://{spark.yarn.historyServer.address}/api/v1/applications/{applicationId}/logs or https://{spark.yarn.historyServer.address}/api/v1/applications/{applicationId}/logs

## 3. Elasticsearch index
```json
POST _template/monitoring-spark-application-audit
{
  "order": 1,
    "index_patterns": [
      "monitoring-spark-application-audit*"
    ],
    "settings": {
      "index": {
        "number_of_shards": "1",
        "number_of_replicas": "1"
        }
      },
    "mappings": {
      "applications": {
        "properties": {
          "allocated_memory": {
            "type": "long"
          },
          "app_end_timestamp": {
            "type": "date"
          },
          "app_id": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "app_name": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "app_start_timestamp": {
            "type": "date"
          },
          "criticality": {
            "type": "long"
          },
          "duration": {
            "type": "long"
          },
          "dynalicAllocation": {
            "type": "boolean"
          },
          "executor_core": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "executor_instances": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "executor_memory": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "nb_tasks": {
            "type": "long"
          },
          "result": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "serializer": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "shuffle_service": {
            "type": "boolean"
          },
          "status_code": {
            "type": "long"
          },
          "used_memory": {
            "type": "long"
          },
          "user": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "wasted_memory": {
            "type": "long"
          },
          "yarn_queue": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          }
        }
      }
    }
}
```

## 4. Configuration 
*  conf/configfile.json is the application configuration (contains : elk_user, elk_host, spark conf location ...)
*  conf/logging_config.ini is logging configuration 

## 5. Launch
Here an example to launch Dr. Spark on the last hour :
```Bash
maxEndDate=$(date -u '+%Y-%m-%dT%H:00:00.000GMT')
minEndDate=$(date -u '+%Y-%m-%dT%H:00:00.000GMT' -d  "1 hour ago")
log_conf=/dlk/home/p085607/notebooks/dr_spark/conf/configfile.json

echo "Password" | kinit user #If kerberos is activate (it is better to use a keytab)
conda activate monitoring
python ./main.py --minEndDate $minEndDate --maxEndDate $maxEndDate --config $log_conf
```


## 6. Reference
*  https://github.com/xiandong79/Spark-Log-Parser
*  https://spark.apache.org/docs/2.3.0/monitoring.html
*  https://github.com/apache/spark