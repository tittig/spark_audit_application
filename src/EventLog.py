"""
@author: Gimenez Thibault
@description: Class to parse spark eventlog
@attr spark_config :  dict of spark configuration (dict)
@attr application_conf : dict of eventLog analysis. It contains configuration, consumption ... (dict)
@attr executors :  dict of executors launched by the application (dict)
@attr jobs : dict of jobs launched by the application (dict)
@attr tasks :  dict of tasks launched by the application (dict)
@attr block_managers : list of block_managers launched by the application (list)
@attr status_code : result of sparkhistory request for the application (int)
@attr _session :  unique session shared by all EventLog object (requests.sessions.Session object)
@attr kerberos_bool : boolean of kerberos status (boolean)
"""
#import
import sys
import zipfile, io
import json
import requests
import time
from requests_kerberos import HTTPKerberosAuth

from .Singleton_logger import *
from .BlockManager import BlockManager
from .Stage import Stage
from .Executor import Executor
from .Job import Job
from .Stage import Stage
from .Task import Task


class EventLog:

    
    def __init__(self,spark_config=None,application_id=None):
        """
        @description: init EventLog object
        @param application_id : id of one spark application to parse 
        @param spark_config : dict contains spark config
        @return : EventLog object
        """
        self.spark_config = spark_config
        self.application_conf = {}
        self.status_code = None
        self.executors = {}
        self.jobs = {}
        self.tasks = {}
        self.block_managers= []
        if spark_config != None :
            self.check_kerberos()
        if not hasattr(type(self), '_session'):
            self._create_session()
        if application_id != None :
            self.get_eventLog(application_id,spark_config)
     
    #check if kerberos is activate
    def check_kerberos(self):
        """
        @description: check if kerberos is enable
        @param spark_config : contains spark sonfig information
        @return : None
        """
        if "spark.history.kerberos.enabled" in self.spark_config['spark_section'] and self.spark_config.get('spark_section','spark.history.kerberos.enabled') == "true" :
            self.kerberos_bool = True
        else : 
            self.kerberos_bool = False
            
    #create session
    @classmethod
    def _create_session(cls):
        """
        @description: create an unique resquest session
        @param : None
        @return : requests.Session()
        """
        MyLogger.__call__().get_logger().debug("Create new session ")
        cls._session = requests.Session()
        cls._session.trust_env = False
        
    #close session
    @classmethod
    def _close_session(cls) :
        """
        @description: close the unique requests.Session()
        @param url : None
        @return : None
        """
        cls._session.close()
    
    #request history server
    @classmethod
    def get_krb(cls,url):
        """
        @description: use the unique requests.Session() to request spark history with Kerberos activate
        @param url :  url to request
        @return : request result
        """
        MyLogger.__call__().get_logger().info("Session : "+ str(cls._session) + " kerberos")
        retries = 5
        while retries:
            try:
                cls._session.auth = HTTPKerberosAuth(mutual_authentication="OPTIONAL")
                r = cls._session.get(url, verify=False, stream=True, timeout=60)
                return r
            except requests.ConnectionError as e:
                last_connection_exception = e
                time.sleep(5)
                retries -= 1
        raise last_connection_exception
    
    #post to elasticsearch
    @classmethod
    def post_elk(cls, url, credential, bulk):
        """
        @description: use the unique requests.Session() to post in elasticsearch
        @param url :  url to request
        @param credential : elasticsearch credential
        @param bulk : data to post in elasticsearch 
        @return : None
        """
        headers = {'Accept': 'application/json' , 'Content-type':'application/json'}
        MyLogger.__call__().get_logger().info("Session : "+ str(cls._session) + " post to elastic")
        cls._session.auth = credential
        retries = 5
        while retries:
            try:
                return cls._session.post(url,data=bulk ,auth=credential,  headers=headers, verify=False, timeout=3)
            except Exception as e :
                last_connection_exception = e
                time.sleep(5)
                retries -= 1
            except:
                time.sleep(5)
                retries -= 1
        raise last_connection_exception
       
        
    #dictionary for eventlog type and call function by type of event
    def eventlog_dict(self,application_id,event_type,json_data):
        """
        @description: dict to root spark event to the good parse function
        @param application_id : id of one spark application to parse 
        @param event_type : type of the spark event
        @param json_data : contains the event data
        @return : function
        """
        switcher={
            "SparkListenerLogStart":lambda:self.do_SparkListenerLogStart(json_data),
            "SparkListenerBlockManagerAdded":lambda:self.do_SparkListenerBlockManagerAdded(json_data),
            "SparkListenerEnvironmentUpdate":lambda:self.do_SparkListenerEnvironmentUpdate(json_data),
            "SparkListenerApplicationStart":lambda:self.do_SparkListenerApplicationStart(json_data),
            "SparkListenerApplicationEnd":lambda:self.do_SparkListenerApplicationEnd(json_data),
            "SparkListenerJobStart":lambda:self.do_SparkListenerJobStart(json_data),
            "SparkListenerStageSubmitted":lambda:self.do_SparkListenerStageSubmitted(json_data),
            "SparkListenerExecutorAdded":lambda:self.do_SparkListenerExecutorAdded(json_data),
            "SparkListenerTaskStart":lambda:self.do_SparkListenerTaskStart(json_data),
            "SparkListenerTaskEnd":lambda:self.do_SparkListenerTaskEnd(json_data),
            "SparkListenerExecutorRemoved":lambda:self.do_SparkListenerExecutorRemoved(json_data),
            "SparkListenerBlockManagerRemoved":lambda:self.do_SparkListenerBlockManagerRemoved(json_data),
            "SparkListenerStageCompleted":lambda:self.do_SparkListenerStageCompleted(json_data),
            "SparkListenerJobEnd":lambda:self.do_SparkListenerJobEnd(json_data),
            "org.apache.spark.sql.execution.ui.SparkListenerSQLExecutionStart":lambda:self.do_SparkListenerSQLExecutionStart(json_data),
            "org.apache.spark.sql.execution.ui.SparkListenerSQLExecutionEnd":lambda:self.do_SparkListenerSQLExecutionEnd(json_data),
            "org.apache.spark.sql.execution.ui.SparkListenerDriverAccumUpdates":lambda:self.do_SparkListenerDriverAccumUpdates(json_data),
            "org.apache.spark.sql.catalyst.catalog.DropTablePreEvent":lambda:self.do_DropTablePreEvent(json_data),
            "org.apache.spark.sql.catalyst.catalog.DropTableEvent":lambda:self.do_DropTableEvent(json_data),
            "org.apache.spark.sql.catalyst.catalog.CreateTableEvent":lambda:self.do_CreateTableEvent(json_data),
            "org.apache.spark.sql.catalyst.catalog.CreateTablePreEvent":lambda:self.do_CreateTablePreEvent(json_data)
         }
        # Get the function from switcher dictionary
        func = switcher.get(event_type, lambda: MyLogger.__call__().get_logger().info(application_id+" : Unknown "+event_type) )
        # Execute the function
        return func() 
        
    #function to get and parse eventlog
    def get_eventLog(self,application_id,spark_config):
        """
        @description: function to parse one spark event 
        @param application_id : id of one spark application to parse 
        @param spark_config : dict of spark conf
        @return : function
        """
        self.application_conf["app_id"] = application_id
        switcher={
            True:lambda:self.get_eventLog_with_kerberos(application_id,spark_config),
            False:lambda:self.get_eventLog_without_kerberos(application_id,spark_config)
        }
        # Get the function from switcher dictionary
        func = switcher.get(self.kerberos_bool)
        # Execute the function
        return func() 
        
    #function to get and parse eventlog in cluster without kerberos
    def get_eventLog_without_kerberos(self,application_id,spark_config):
        """
        @description: function to parse one spark event in cluster without security
        @param application_id : id of one spark application to parse 
        @param spark_config : dict of spark conf
        @return : None
        """
        MyLogger.__call__().get_logger().info("Kerberos isn't activate")
        MyLogger.__call__().get_logger().info("No Kerberos part isn't implemented")
        #TO DO
        pass
    
    #function to get and parse eventlog in kerberos cluster
    def get_eventLog_with_kerberos(self,application_id,spark_config):
        """
        @description: function to parse one spark event in cluster with kerberos
        @param application_id : id of one spark application to parse 
        @param spark_config : dict of spark conf
        @return : None
        """
        #create session to request history server
        host = spark_config.get('spark_section','spark.yarn.historyServer.address')
        url = "https://"+host+"/api/v1/applications/"+application_id+"/logs"
        try :
            MyLogger.__call__().get_logger().info("Requests : "+url)
            r = self.get_krb(url)
            MyLogger.__call__().get_logger().info("Requests : "+url+" "+str(r.status_code) )
            self.status_code = r.status_code 
            if r.status_code == requests.codes.ok :
                #read file download from spark history 
                z = zipfile.ZipFile(io.BytesIO(r.content))
                #z.extractall()
                r.close
                MyLogger.__call__().get_logger().info("Read eventlog "+str(application_id)+" : "+str(z.namelist()))
                #read only the last appattempt log
                z.namelist().sort(reverse = True)
                z.namelist()[0]
                MyLogger.__call__().get_logger().info("Read eventlog "+application_id +" : "+ z.namelist()[0])
                with z.open( z.namelist()[0]) as f: 
                    for line in f:
                        json_data = json.loads(line)
                        event_type = json_data["Event"]
                        MyLogger.__call__().get_logger().debug(application_id+" : parse event "+event_type)
                        #use dictionary to call function
                        self.eventlog_dict(application_id,event_type,json_data)     
        except :
            MyLogger.__call__().get_logger().info(str(sys.exc_info()[0])+" "+str(sys.exc_info()[1])+ " Failed read eventlog : "+ application_id)
            e = sys.exc_info()[1]
        

    #Parse SparkListenerLogStart event
    def do_SparkListenerLogStart(self, data):
        """
        @description: function to parse SparkListenerLogStart event 
        @param data : contains the event data
        @return : None
        """
        self.application_conf["spark_version"] = data["Spark Version"]
    
    #Parse SparkListenerBlockManagerAdded event
    def do_SparkListenerBlockManagerAdded(self, data):
        """
        @description: function to parse SparkListenerBlockManagerAdded event 
        @param data : contains the event data
        @return : None
        """
        bm = BlockManager(data)
        self.block_managers.append(bm)
    
    #Parse SparkListenerEnvironmentUpdate event
    def do_SparkListenerEnvironmentUpdate(self, data):
        """
        @description: function to parse SparkListenerEnvironmentUpdate event 
        @param data : contains the event data
        @return : None
        """
        self.application_conf["java_version"] = data.get("JVM Information", None).get("Java Version", None)
        self.application_conf["app_name"] = data.get("Spark Properties", None).get("spark.app.name", None)
        self.application_conf["yarn_queue"] = data.get("Spark Properties",None).get("spark.yarn.queue",None)       
        self.application_conf["serializer"] = data.get("Spark Properties",None).get("spark.serializer", "default")
        self.application_conf["shuffle_service"] = data.get("Spark Properties",None).get("spark.shuffle.service.enabled",False)
        self.application_conf["dynalicAllocation"] = data.get("Spark Properties",None).get("spark.dynamicAllocation.enabled",False)
        self.application_conf["dynamicAlloc_maxExec"] = data.get("Spark Properties",None).get("spark.dynamicAllocation.maxExecutors",None)
        self.application_conf["driver_memory"] = data.get("Spark Properties",None).get("spark.driver.memory","default")
        self.application_conf["executor_instances"] = data.get("Spark Properties",None).get("spark.executor.instances","default")
        self.application_conf["executor_memory"] = data.get("Spark Properties",None).get("spark.executor.memory","default")
        self.application_conf["oozie_id"] = data.get("Spark Properties",None).get("spark.oozie.job.id","default")
        self.application_conf["executor_core"] = data.get("Spark Properties",None).get("spark.executor.cores","default")
        
    #Parse SparkListenerApplicationStart event
    def do_SparkListenerApplicationStart(self, data):
        """
        @description: function to parse SparkListenerApplicationStart event 
        @param data : contains the event data
        @return : None
        """
        self.application_conf["app_start_timestamp"] = data["Timestamp"]
        self.application_conf["user"] = data["User"]
        
    #Parse SparkListenerApplicationEnd event
    def do_SparkListenerApplicationEnd(self, data):
        """
        @description: function to parse SparkListenerApplicationEnd event 
        @param data : contains the event data
        @return : None
        """
        self.application_conf["app_end_timestamp"] = data["Timestamp"]    
     
    #Parse SparkListenerJobStart event
    def do_SparkListenerJobStart(self, data):
        """
        @description: function to parse SparkListenerJobStart event 
        @param data : contains the event data
        @return : None
        """
        job_id = data["Job ID"]
        if job_id in self.jobs:
            print("WARN: Duplicate job ID!")
            return
        job = Job(data) # that class Job
        self.jobs[int(job_id)] = job # record into the `dict`
 
    #Parse SparkListenerStageSubmitted event
    def do_SparkListenerStageSubmitted(self, data):
        """
        @description: function to parse SparkListenerStageSubmitted event 
        @param data : contains the event data
        @return : None
        """
        pass        

    #Parse SparkListenerExecutorAdded event
    def do_SparkListenerExecutorAdded(self, data):
        """
        @description: function to parse SparkListenerExecutorAdded event 
        @param data : contains the event data
        @return : None
        """
        exec_id = data["Executor ID"]
        self.executors[exec_id] = Executor(data)
        
    #Parse SparkListenerTaskStart event
    def do_SparkListenerTaskStart(self, data):
        """
        @description: function to parse SparkListenerTaskStart event 
        @param data : contains the event data
        @return : None
        """
        task_id = data["Task Info"]["Task ID"]
        self.tasks[int(task_id)] = Task(data)

    #Parse SparkListenerTaskEnd event
    def do_SparkListenerTaskEnd(self, data):
        """
        @description: function to parse SparkListenerTaskEnd event 
        @param data : contains the event data
        @return : None
        """
        task_id = data["Task Info"]["Task ID"]
        self.tasks[int(task_id)].finish(data)
        
    #Parse SparkListenerExecutorRemoved event
    def do_SparkListenerExecutorRemoved(self, data):
        """
        @description: function to parse SparkListenerExecutorRemoved event 
        @param data : contains the event data
        @return : None
        """
        exec_id = data["Executor ID"]
        self.executors[exec_id].remove(data)

    #Parse SparkListenerStageCompleted event
    def do_SparkListenerStageCompleted(self, data):
        """
        @description: function to parse SparkListenerStageCompleted event 
        @param data : contains the event data
        @return : None
        """
        stage_id = data["Stage Info"]["Stage ID"]
        for j in self.jobs.values():
            for s in j.stages: # class Stage in job.py
                if s.stage_id == stage_id:
                    s.complete(data)

    #Parse SparkListenerJobEnd event
    def do_SparkListenerJobEnd(self, data):
        """
        @description: function to parse SparkListenerJobEnd event 
        @param data : contains the event data
        @return : None
        """
        job_id = data["Job ID"]
        self.jobs[job_id].complete(data)
        
        #Parse SparkListenerBlockManagerRemoved
    def do_SparkListenerBlockManagerRemoved(self, data):
        pass
    
    #Parse SparkListenerSQLExecutionEnd event
    def do_SparkListenerSQLExecutionEnd(self, data):
        pass
    
    #Parse SparkListenerSQLExecutionStart event
    def do_SparkListenerSQLExecutionStart(self, data):
        pass
    
    #Parse SparkListenerDriverAccumUpdates event
    def do_SparkListenerDriverAccumUpdates(self, data):
        pass

    #Parse DropTablePreEvent event
    def do_DropTablePreEvent(self, data):
        pass
    
    #Parse DropTable event
    def do_DropTableEvent(self, data):
        pass

    #Parse CreateTablePre event
    def do_CreateTablePreEvent(self, data):
        pass

    #Parse CreateTable event
    def do_CreateTableEvent(self, data):
        pass
    
    def basic_SparkEvent_audit(self): 
        """
        @description: function to compute simple heuristic 
        @param : None
        @return : dict of result
        """
        weight_ok = 300000000000000 # maximum weight where criticality is ok (arbitrary value)
        weight_memory = 0
        weight_conf = 0
        audit = {'app_id':None, 'app_name':None, 'duration':None, 'yarn_queue':None, 'user':None, 'driver_memory':None, 'executor_instances':None, 'executor_memory':None, 'executor_core':None, 'oozie_id':None, 'dynalicAllocation':None, 'dynamicAlloc_maxExec':None, 'serializer':None, 'shuffle_service':None,'app_start_timestamp':None, 'app_end_timestamp':None, 'nb_tasks':None, 'result_size':None, 'input_bytes':None, 'output_write':None, 'storage_memory':None, 'shuffle_memory':None, 'allocated_memory':None, 'used_memory':None, 'wasted_memory':None, 'peak_memory_mean':None, 'memory_bytes_spilled':None, 'disk_bytes_spilled':None, 'shuffle_read':None, 'shuffle_write':None, 'result': None, 'status_code':None, 'weight':None,'criticality':None}
        
        audit['status_code'] = self.status_code
        
        if self.status_code == 200 :
            critic = 0 # Ranges from 0(LOW) to 4(CRITICAL)'
            totalBytes = 0 
            #configuration information
            audit['app_id'] = self.application_conf['app_id']
            audit['app_name'] = self.application_conf.get('app_name',None)
            audit['duration'] = self.application_conf.get('app_start_timestamp',None)
            audit['yarn_queue'] = self.application_conf.get('yarn_queue',None)
            audit['user'] = self.application_conf.get('user',None)
            audit['driver_memory'] = self.application_conf.get('driver_memory',0)
            audit['executor_instances'] = self.application_conf.get('executor_instances',0)
            audit['executor_memory'] = self.application_conf.get('executor_memory',0)
            audit['executor_core'] = self.application_conf.get('executor_core',0)
            audit['oozie_id'] = self.application_conf.get('oozie_id',None)
            audit['dynalicAllocation'] = self.application_conf.get('dynalicAllocation',None)
            audit['dynamicAlloc_maxExec'] = self.application_conf.get('dynamicAlloc_maxExec',None)
            audit['serializer'] = self.application_conf.get('serializer',None)
            audit['shuffle_service'] = self.application_conf.get('shuffle_service',None)
            #job information 
            audit['app_start_timestamp'] = self.application_conf.get('app_start_timestamp',0)
            audit['app_end_timestamp'] = self.application_conf.get('app_end_timestamp',0)
            if 'app_end_timestamp' in self.application_conf :
                audit['duration'] = int(audit['app_end_timestamp'])-int(audit['app_start_timestamp']) 
            else :
                audit['duration'] = 0
            audit['nb_tasks'] = len(self.tasks)
            audit['result_size'] = sum(self.tasks[b].result_size for b in self.tasks)
            audit['input_bytes'] = sum(self.tasks[b].input for b in self.tasks)
            audit['output_write'] = sum(self.tasks[b].output_write for b in self.tasks)
            #memory information
            audit['storage_memory'] = sum(b.maximum_memory for b in self.block_managers)
            audit['shuffle_memory'] = sum(self.tasks[b].peak_execution_memory for b in self.tasks)
            audit['allocated_memory'] =  audit['storage_memory']/0.6 #default value spark.memory.fraction
            count = 1
            if  self.tasks !=0 :
                for i in self.tasks :
                    audit['peak_memory_mean'] = int(0 if audit['peak_memory_mean'] is None else audit['peak_memory_mean'])+ self.tasks[i].peak_execution_memory
                    if self.tasks[i].peak_execution_memory !=0 :
                        count = count + 1
            audit['peak_memory_mean'] = int(0 if audit['peak_memory_mean'] is None else audit['peak_memory_mean'] )/count
            audit['used_memory'] = int(audit['input_bytes']) + int(audit['peak_memory_mean'])*int(2 if audit['executor_instances'] is "default" else audit['executor_instances'])*int(1 if audit['executor_core']  is "default" else audit['executor_core'])
            audit['wasted_memory'] = audit['allocated_memory'] - audit['used_memory']
            audit['memory_bytes_spilled'] = sum(self.tasks[b].memory_bytes_spilled for b in self.tasks)
            #disk information
            audit['shuffle_read'] = sum(self.tasks[b].shuffle_read for b in self.tasks)
            audit['shuffle_write'] = sum(self.tasks[b].shuffle_write for b in self.tasks)
            audit['disk_bytes_spilled'] = sum(self.tasks[b].disk_bytes_spilled  for b in self.tasks)
            #compute weight and criticality
            weight_memory = audit['wasted_memory']*audit['duration']
            if audit['shuffle_service'] == False or audit['serializer']!="org.apache.spark.serializer.KryoSerializer": 
                weight_conf=weight_ok + 1
            if audit['dynalicAllocation'] != False and audit['dynamicAlloc_maxExec'] == None:
                weight_conf=weight_ok*10
            if (len(self.jobs.keys())) != 0 :
                audit['result'] = self.jobs[max(self.jobs.keys())].result
            else :
                audit['result'] = None
            audit['weight'] = weight_conf + weight_memory           
            if  audit['weight'] >= 0 and audit['weight'] < 300000000000001 :
                audit["criticality"] = 0
            else :
                if  audit['weight'] < 0 :
                    audit["criticality"] = 3
                else : 
                    if audit['weight'] >= 300000000000001 and audit['weight'] < 600000000000000 :
                        audit["criticality"] = 1
                    else :
                        if audit['weight'] >= 600000000000000 and audit['weight'] < 1000000000000000:
                            audit["criticality"] = 2
                        else :
                            if audit['weight'] >= 1000000000000000 :
                                audit["criticality"] = 4
        return audit
    
