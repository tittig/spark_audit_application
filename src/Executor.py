"""
@author: Gimenez Thibault
@description: It uses SparkListenerExecutorAdded and SparkListenerExecutorRemoved event to create Executor object from spark executor
"""
"""
spark.shuffle.memoryFraction	0.2
spark.storage.memoryFraction	0.6
spark.executor.memoryOverhead	executorMemory * 0.10, with minimum of 384
"""

from datetime import datetime

class Executor:

    def __init__(self, data):
        """
        @description: init Executor object from SparkListenerExecutorAdded
        @param data : json event 
        @return : Executor object
        """
        #Initialized with SparkListenerExecutorAdded
        self.executor_id = data["Executor ID"]
        self.host = data["Executor Info"]["Host"]
        self.block_managers = [] # block_managers belong to
        self.start_timestamp = data["Timestamp"]
        self.total_cores = int(data["Executor Info"]["Total Cores"])
        
        self.tasks = []
        self.remove_reason = None
        self.remove_timestamp = None

    def add_block_manager(self, bm):
        """
        @description: add associat BlockManager
        @param data : BlockManager object
        @return : Executor object
        """
        self.block_managers.append(bm)

    #launched if we have SparkListenerExecutorRemoved event
    def remove(self, data):
        """
        @description: complet Executor object from SparkListenerExecutorRemoved
        @param data : json event 
        @return : Executor object
        """
        self.remove_reason = data["Removed Reason"]
        self.remove_timestamp = data["Timestamp"]

    
    def __repr__(self):
        return str(self)
    
    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)