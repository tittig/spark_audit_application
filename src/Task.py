"""
@author: Gimenez Thibault
@description: It uses SparkListenerTaskStart and SparkListenerTaskEnd event to create Task object from spark task
"""

from datetime import datetime

class Task:

    def __init__(self, data):
        """
        @description: init Task object from SparkListenerTaskStart
        @param data : json event 
        @return : Task object
        """
        #initialized with SparkListenerTaskStart
        self.task_id = data["Task Info"]["Task ID"]
        self.stage_id = data["Stage ID"]
        self.executor_id = data["Task Info"]["Executor ID"]
        self.launch_time = data["Task Info"]["Launch Time"]
        self.locality = data["Task Info"]["Locality"]
        self.speculative = data["Task Info"]["Speculative"]

        #initialized with SparkListenerTaskEnd
        self.end_reason = None
        self.failed = False
        self.finish_time = None
        self.getting_result_time = None
        self.index = None
        self.type = None
        self.has_metrics = False
        self.executor_run_time = None
        self.jvm_gc_time = None
        self.result_size = 0
        self.input = 0
        self.input_records_read = 0
        self.shuffle_read = 0
        self.shuffle_write = 0
        self.output_write = 0
        self.memory_bytes_spilled = 0
        self.disk_bytes_spilled = 0
        self.peak_execution_memory = 0

    def finish(self, data):
        """
        @description: complet Task object from SparkListenerTaskEnd
        @param data : json event
        @return : Task object
        """
        self.end_reason = data["Task End Reason"]["Reason"]
        self.failed = data["Task Info"]["Failed"] 
        self.finish_time = data["Task Info"]["Finish Time"] 
        self.getting_result_time = data["Task Info"]["Getting Result Time"] 
        self.index = data["Task Info"]["Index"]
        self.type = data["Task Type"]
        for el in data["Task Info"]["Accumulables"] :
            if el["Name"] == "internal.metrics.peakExecutionMemory" :
                self.peak_execution_memory = el["Update"] #Peak Execution memory refers to the memory used by internal data structures created during shuffles, aggregations and joins
                
            
        if "Task Metrics" in data:
            self.has_metrics = True
            self.executor_run_time = data["Task Metrics"]["Executor Run Time"] #Elapsed time the executor spent running this task. This includes time fetching shuffle data. The value is expressed in milliseconds
            self.jvm_gc_time = data["Task Metrics"]["JVM GC Time"] #Elapsed time the JVM spent in garbage collection while executing this task. The value is expressed in milliseconds.
            self.result_size = data["Task Metrics"]["Result Size"] # The number of bytes this task transmitted back to the driver as the TaskResult.
            self.input = data["Task Metrics"]["Input Metrics"]["Bytes Read"] # Metrics related to reading data from [[org.apache.spark.rdd.HadoopRDD]] or from persisted data. bytesRead => Total number of bytes read.
            self.shuffle_read = data["Task Metrics"]["Shuffle Read Metrics"]["Local Bytes Read"] # Number of bytes read in shuffle operations from local disk
            self.shuffle_write = data["Task Metrics"]["Shuffle Write Metrics"]["Shuffle Bytes Written"] # Number of bytes written in shuffle operations
            self.input_records_read = data["Task Metrics"]["Input Metrics"]["Records Read"] # Total number of records read
            self.output_write = data["Task Metrics"]["Output Metrics"]["Bytes Written"] # Metrics related to writing data externally (e.g. to a distributed filesystem), defined only in tasks with output
            self.memory_bytes_spilled = data["Task Metrics"]["Memory Bytes Spilled"] # Shuffle spill (memory) - size of the deserialized form of the data in memory at the time of spilling
            self.disk_bytes_spilled = data["Task Metrics"]["Disk Bytes Spilled"] #shuffle spill (disk) - size of the serialized form of the data on disk after spilling 
            

            
    def __repr__(self):
        return str(self)
            
    def __str__(self): 
        return str(self.__class__) + ": " + str(self.__dict__)