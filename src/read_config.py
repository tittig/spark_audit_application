"""
@author: Gimenez Thibault
@description: Read configuration
"""

import json
import configparser

class read_conf():
    def __init__(self):
        pass

    def read_configuration_file(self, configuration_file_name):
        """
        @description: read a json configuration file
        @param configuration_file_name : configuration file name
        @return : the configuration object
        """
        with open(configuration_file_name, "r") as config_file:
            configuration_info = json.load(config_file)
        return configuration_info
    
    def load_json_config(self, arg):
        """
        @description : load configuration from config json files
        @param arg : configuration file name
        @return : configuration parameters
        """
        app_configuration_file = arg
        app_configs = self.read_configuration_file(app_configuration_file)
        return app_configs
    
    def read_spark_conf(self,path) :
        """
        @description : load configuration from spark-defaults.conf
        @param path : spark conf path
        @return : configparser
        """
        with open(path) as f:
            file_content = '[spark_section]\n' + f.read()

        config_parser = configparser.ConfigParser(delimiters=' ')
        config_parser.read_string(file_content)
        return config_parser


    

    