"""
@author: Gimenez Thibault
@description: Logger singleton 
"""

import configparser
import logging
import logging.handlers as handlers
import logging.config


class SingletonType(type):

    _instances = {}

    def __call__(cls, *args, **kwargs):
        """
        @description: create singleton
        @return : singleton
        """
        if cls not in cls._instances:
            cls._instances[cls] = super(SingletonType, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
    

class MyLogger(object, metaclass=SingletonType):

    _logger = None

    def __init__(self):
        """
        @description: create logger singleton
        @return : logger singleton
        """
        logging.config.fileConfig('/dlk/home/p085607/notebooks/dr_spark/conf/logging_config.ini')
        self._logger = logging.getLogger('applicationLogger')

    def get_logger(self):
        return self._logger