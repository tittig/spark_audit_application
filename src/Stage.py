"""
@author: Gimenez Thibault
@description: It uses SparkListenerStageSubmitted and SparkListenerStageCompleted event to create Stage object from spark stage
"""

class Stage:
    def __init__(self, stage_data):
        """
        @description: init Stage object from SparkListenerStageCompleted 
        @param data : json event
        @return : Stage object
        """
        self.stage_id = stage_data["Stage ID"]
        self.task_num = int(stage_data["Number of Tasks"])
        self.name = stage_data["Stage Name"]

        self.completion_time = None
        self.submission_time = None

    def complete(self, data):
        """
        @description: init Stage object from SparkListenerStageCompleted
        @param data : json event 
        @return : Stage object
        """
        self.completion_time = data["Stage Info"]["Completion Time"]
        self.submission_time = data["Stage Info"]["Submission Time"]
   
    def __repr__(self):
        return str(self)
    
    def __str__(self): 
        return str(self.__class__) + ": " + str(self.__dict__)