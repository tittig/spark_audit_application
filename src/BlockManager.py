"""
@author: Gimenez Thibault
@description: It uses  SparkListenerBlockManagerAdded and SparkListenerBlockManagerRemoved event to create BlockManager object from spark blockManager
"""

class BlockManager:
    def __init__(self, data):
        """
        @description: init BlockManager object from SparkListenerBlockManagerAdded
        @param data : json event 
        @return : BlockManager object
        """
        self.maximum_memory = int(data["Maximum Memory"])
        self.host = data["Block Manager ID"]["Host"]
        self.add_timestamp = data["Timestamp"]
        self.executor_id = data["Block Manager ID"]["Executor ID"]

    def __repr__(self):
        return str(self)
    
    def __str__(self): 
        return str(self.__class__) + ": " + str(self.__dict__)