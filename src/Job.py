"""
@author: Gimenez Thibault
@description: It uses SparkListenerJobStart and SparkListenerJobEnd event to create Job object from spark job
"""

from .Stage import Stage

class Job:  
    def __init__(self, start_data):
        """
        @description: init Job object from SparkListenerJobStart
        @param data : json event 
        @return : Job object
        """
        # you can find a line with event_type = "SparkListenerJobStart" and copy it into https://jsonformatter.org
        # see the hierarchical structure
        self.job_id = int(start_data["Job ID"])
        self.stages = start_data["Stage IDs"]

        #update with SparkListenerStageCompleted event
        self.stages = []
        for stage_data in start_data["Stage Infos"]:
            self.stages.append(Stage(stage_data))   # class Stage
        self.submission_time = start_data["Submission Time"]
        self.result = None
        self.end_time = None

    #SparkListenerJobStart event
    def complete(self, data):
        """
        @description: complet Job object from SparkListenerJobEnd
        @param data : json event 
        @return : Job object
        """
        self.result = data["Job Result"]["Result"]
        self.end_time = data["Completion Time"]
        
    def __repr__(self):
        return str(self)
        
    def __str__(self): 
        return str(self.__class__) + ": " + str(self.__dict__)